package com.company;

public class Main {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        System.out.println(calculator.divide(8965, 2));
        System.out.println(calculator.sum(2423, 23));
        System.out.println(calculator.minus(21355, 255));
        System.out.println(calculator.multiply(4234, 2340));
        System.out.println(calculator.divide(85, 2));
        System.out.println(calculator.sum(3, 23));
        System.out.println(calculator.minus(21, 5));
        System.out.println(calculator.multiply(44, 20));
        System.out.println(calculator.panda(44, 20, 11));

    }
}